﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatGame.GameClasses.Objects {
	public enum Direction :byte{
		up,
		down,
		left,
		right
	}

	abstract public class MovingObject : GameObject{
		#region MOVEMENT_VAR
		public float moveAcceleration;     // acceleration on moving
		public bool stopping;
		public bool falling;
		
		#endregion

		#region ANIMATION_VAR
		public float accumulator;   // accumulator for gametime (animation)
		#endregion

		#region FUNCTIONS
		public MovingObject(float x, float y){
			this.x = x;
			this.y = y;
			this.accumulator = 0f;
			moveAcceleration = 0f;
			_vX = 0;
			_vY = 0;
			falling = true;
			stopping = false;
		}

		#region MOVEMENT_FUNC
		public virtual void Update(GameTime gametime, Level level) {
			base.Update(gametime, level);

			if (correctCollisions(level)) { //returns true when on the ground
				falling = false;
				if (stopping) { // if you'd like to stop, apply friction
					applyFriction(gametime.ElapsedGameTime.Milliseconds);
				}
			}
			else {
				falling = true;
				vY += GRAVITY * gametime.ElapsedGameTime.Milliseconds / 1000f;
			}

		}

		public bool correctCollisions(Level level) {
			// check if boundary pixels overlap, and correct
			int i = 0; // walks over the pixels of the edge
			int j = 0; // walk over depth layers
			int intX = (int)x;
			int intY = (int)y;
			int detectDepthX = (int)width / 5; // how deep to check inside object, on X axis
			int detectDepthY = (int)height / 5; // how deep to check inside object, on Y axis
				//int rampUp = 4; // how steep an incline you can walk up currently using detectDepth instead
			bool breakout; // tfw c# has no break 2
			bool onGround = false;


			// check the edges, corners may overlap, they don't matter that much and simplify things immensly
			// top first so you bump your head 
			breakout = false;
			for (j = 0; j < detectDepthY; j++) {
				for (i = detectDepthX; i < (width - detectDepthX); i++) { // check over a line at the top, but skip the edges
					if (level.layout[intX + i, intY + j] == Tile.Solid) {
						//*DEBUG*/ Console.WriteLine($"encountered at {intX + i}, {intY + j}");
						y = y + j + 1;
						vY = -vY / 3;	//bounce
						breakout = true;
						break;
					}
				}
				if (breakout) { break; }
			}
			// left overlap 
			breakout = false;
			for (j = detectDepthX - 1; j >= 0; j--) {               // start deepest inside, pushing outwards
				for (i = detectDepthY; i < (height - detectDepthY); i++) {      // don't need to check on ramp height
					if (level.layout[intX + j, intY + i] == Tile.Solid) { //encountered a solid
						//*DEBUG*/Console.WriteLine($"encountered at {intX + j}, {intY + i}");
						x = x + j + 1; // move to the right
						vX = -vX / 3; // bounce?
						breakout = true;
						break;
					}
				}
				if (breakout) { break; }
			}

			// right overlap 
			breakout = false;
			for (j = 0; j < detectDepthX; j++) {
				for (i = detectDepthY; i < (height - detectDepthY); i++) {
					if (level.layout[intX + (int)width - j, intY + i] == Tile.Solid) { //encountered a solid
						//*DEBUG*/Console.WriteLine($"encountered at {intX + (int)width - j}, {intY + i}");
						x = x - j - 1; // move to the left
						vX = -vX / 3; // bounce
						breakout = true;
						break;
					}
				}
				if (breakout) { break; }
			}
			// bottom overlap
			breakout = false;
			for (j = 0; j < detectDepthY; j++) {
				for (i = detectDepthX; i < (width - detectDepthX); i++) {
					if (level.layout[intX + i, intY + (int)height - j] == Tile.Solid) {
						//*DEBUG*/Console.WriteLine($"encountered at {intX + j}, {intY + i}");
						y = y - j - 1;
						intY = (int)y;
						vY = 0;
						breakout = true;
						break;
					}
				}
				if (breakout) {
					break;
				}
			}

			//also, return true when on the ground. if the pixel underneah is floor, we're on the ground
			for (i = detectDepthX; i < (width - detectDepthX); i++) {
				if (level.layout[intX + i, intY + (int)height + 1] == Tile.Solid) {
					onGround = true;
				}
			}
			return onGround;
		}

		public void applyFriction(int timespan) {
			vY = 0;
			if (vX > 0) {
				vX = Math.Max(0, vX - FRICTION * timespan / 1000f);
			}
			else if (vX < 0) {
				vX = Math.Min(0, vX + FRICTION * timespan / 1000f);
			}
		}
		#endregion MOVEMENT_FUNC
		#endregion FUNCTIONS
	}
}
