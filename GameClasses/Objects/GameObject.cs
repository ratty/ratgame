﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatGame.GameClasses.Objects {
	public abstract class GameObject {
		#region CONSTANTS 
		public static int GRAVITY = 300;
		public static int MAXVELOCITY = 400;
		public static int FRICTION = 500;
		#endregion

		#region MOVEMENT_VAR
		public float x;				//x coordinate
		public float y;				// y coordinate
		public float width;         // width of bounding box
		public float height;        // height of bounding box
		protected float _vX;			// X velocity
		public float vX
		{
			get { return _vX; }
			set
			{
				if (value > 0) {
					_vX = Math.Min(MAXVELOCITY, value);
				}
				else {
					_vX = Math.Max(-MAXVELOCITY, value);
				}
			}
		}
		protected float _vY;			// Y velocity
		public float vY
		{
			get { return _vY; }
			set
			{
				if (value > 0) {
					_vY = Math.Min(MAXVELOCITY, value);
				}
				else {
					_vY = Math.Max(-MAXVELOCITY, value);
				}
			}
		}
		#endregion MOVEMENT_VAR

		#region FUNCTIONS
		public virtual void Update(GameTime gametime, Level level) {
			x += vX * gametime.ElapsedGameTime.Milliseconds / 1000f;
			y += vY * gametime.ElapsedGameTime.Milliseconds / 1000f;
		}

		public abstract void Draw(GameTime gametime, SpriteBatch spritebatch, Texture2D sprite);
		#endregion FUNCTIONS
	}
}
