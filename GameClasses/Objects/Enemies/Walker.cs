﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RatGame.GameClasses.Objects.Enemies {
	class Walker : Enemy {
		public Walker(float x, float y) : base(x, y) {
			width = 26;
			height = 38;
			moveAcceleration = 300;
		}

		public override void Behavior(GameTime gametime, Level level) {
			int intent;	// the direction i want to go. 1 = right, -1 = left
			if (level.mobiles[0].x > this.x) {
				intent = 1;
			}
			else intent = -1;

			vX += moveAcceleration * intent * gametime.ElapsedGameTime.Milliseconds / 1000f;

		}
	}
}
