﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using RatGame.GameClasses.Objects;

namespace RatGame.GameClasses.Objects.Enemies {
	

	public abstract class Enemy : MovingObject {
		

		public Enemy(float x, float y) : base(x, y) {
		}

		public override void Update(GameTime gametime, RatGame.Level level) {
			this.Behavior(gametime,level);

			base.Update(gametime, level);
		}

		public override void Draw(GameTime gametime, SpriteBatch spritebatch, Texture2D sprite) {
			//TODO: move this to an animator class
			spritebatch.Draw(
				sprite,
				new Vector2(x, y + 15),
				new Rectangle(238, 3, 20, 24),
				Color.White,
				0,
				Vector2.Zero,
				1f,
				SpriteEffects.None,
				0f);

			spritebatch.Draw(
				sprite,
				new Vector2(x, y),
				new Rectangle(2, 3, 26, 26),
				Color.White,
				0,
				Vector2.Zero,
				1f,
				SpriteEffects.None,
				0f);

			//spritebatch.DrawString(, "test", new Vector2(3, 3), Color.Black);
		}

		public abstract void Behavior(GameTime gametime, RatGame.Level level);
	}
}
