﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace RatGame.GameClasses.Objects { 
	public enum Actions : byte {
		Left,
		Right,
		Up,
		Down,
		Jump,
		Fire,
		Swap
	}

	public class Player : MovingObject{
		Dictionary<Actions, Keys> inputDict;
		KeyboardState prevKBState;
		KeyboardState currKBState;
		

		public Player(float x,float y) : base(x, y) {
			width = 26;
			height = 38;
			moveAcceleration = 300;
			prevKBState = new KeyboardState();
			currKBState = new KeyboardState();

			inputDict = new Dictionary<Actions, Keys>();
			inputDict.Add(Actions.Left, Keys.Left);
			inputDict.Add(Actions.Right, Keys.Right);
			inputDict.Add(Actions.Jump, Keys.Up);
		}

		public override void Update(GameTime gametime, RatGame.Level level) {
			this.handleInput(gametime);

			base.Update(gametime, level);
		}

		public void handleInput(GameTime gametime) {
			prevKBState = currKBState;
			currKBState = Keyboard.GetState();

			if (currKBState.IsKeyDown(inputDict[Actions.Left])) {
				stopping = false;
				// if you have speed in the opposite direction ,use friction to slow down extra
				if (!falling && vX > 0) applyFriction(gametime.ElapsedGameTime.Milliseconds);
				vX -= moveAcceleration * gametime.ElapsedGameTime.Milliseconds / 1000f;
			}       // moving left and right are mutually exclusive
			else if (currKBState.IsKeyDown(inputDict[Actions.Right])) {
				stopping = false;
				// if you have speed in the opposite direction ,use friction to slow down extra
				if (!falling && vX < 0) applyFriction(gametime.ElapsedGameTime.Milliseconds);
				vX += moveAcceleration * gametime.ElapsedGameTime.Milliseconds / 1000f;
			}
			else { // if you're not moving, express intent to stop
				stopping = true;
			}

			if (currKBState.IsKeyDown(inputDict[Actions.Jump])) {
				// launch yo self
				if(!falling) vY = -MAXVELOCITY;
			}
		}

		public override void Draw(GameTime gametime, SpriteBatch spritebatch, Texture2D sprite) {
			//TODO: move this to an animator class
			spritebatch.Draw(
				sprite,
				new Vector2(x, y+15),
				new Rectangle(238, 3, 20, 24),
				Color.White,
				0,
				Vector2.Zero,
				1f,
				SpriteEffects.None,
				0f);

			spritebatch.Draw(
				sprite,
				new Vector2(x, y),
				new Rectangle(2, 3, 26, 26),
				Color.White,
				0,
				Vector2.Zero,
				1f,
				SpriteEffects.None,
				0f);

			//spritebatch.DrawString(, "test", new Vector2(3, 3), Color.Black);
		}
	}
}
