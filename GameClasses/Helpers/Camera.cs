﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatGame.GameClasses.Helpers {
	public class Camera {
		public float Zoom { get; set; }
		public float Rotation { get; set; }
		public float LocX { get; set; }
		public float LocY { get; set; }
		public float Width { get; set; }
		public float Height { get; set; }
		public Matrix CameraMatrix { get; set; }

		public Camera(Viewport VP) {
			Zoom = 1;
			Rotation = 0;
			LocX = 0;
			LocY = 0;
			Width = VP.Width;
			Height = VP.Height;
		}

		public void BuildMatrix(){
			CameraMatrix = Matrix.CreateTranslation(-LocX, -LocY, 0f) *
				Matrix.CreateRotationZ(Rotation) *
				Matrix.CreateScale(Zoom) *
				Matrix.CreateTranslation(Width * 0.5f, Height * 0.75f, 0);
		}
	}
}
