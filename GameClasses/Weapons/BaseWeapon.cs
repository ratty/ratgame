﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatGame.GameClasses.Weapons {
	public abstract class BaseWeapon {
		public int Ammo;
		public int MaxAmmo;
		public float cooldown;
		public float cdCounter; // cooldown counter
		public bool firing;
		public int x;		// x and Y coordinate? may be removed
		public int y;
		public float direction; // angle to pointing straight right

		

		// base implementation, can be overridden
		public virtual void Trigger(Level level, GameTime gametime) { 
			if(cdCounter == 0f) {
				firing = true;
				cdCounter = cooldown;
			}
		}
		

		public abstract void Update(Level level, GameTime gametime);
	}
}
