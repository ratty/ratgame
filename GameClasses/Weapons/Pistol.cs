﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatGame.GameClasses.Weapons {
	class Pistol : BaseWeapon {

		public override void Trigger(Level level, GameTime gametime) {
			firing = true;
		}

		public override void Update(Level level, GameTime gametime) {
			if (firing) {
				new SimpleBullet(0f, 0f, 0f);

			}
		}
	}
}
