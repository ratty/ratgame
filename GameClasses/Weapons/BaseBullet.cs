﻿using RatGame.GameClasses.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RatGame.GameClasses.Weapons {
	public class SimpleBullet : GameObject {
		public SimpleBullet(float x, float y, float direction) {
			width = 5;
			height = 5;
			this.x = x;
			this.y = y;
		}

		public override void Draw(GameTime gametime, SpriteBatch spritebatch, Texture2D sprite) {
			spritebatch.Draw(
				sprite,
				new Vector2(x, y),
				new Rectangle(0, 0, (int)width, (int)height),
				Color.White,
				0,
				Vector2.Zero,
				1f,
				SpriteEffects.None,
				0f);
		}
	}
}
