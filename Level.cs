﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RatGame.GameClasses.Helpers;
using RatGame.GameClasses.Objects;
using RatGame.GameClasses.Objects.Enemies;
using RatGame.GameClasses.Weapons;
using System;
using System.Collections.Generic;

namespace RatGame {
	public enum Tile {     // the tiles that make up a level:
		Air = 0,	// nothing
		Solid = 1	// Solid block
	}

	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class Level : Game {
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		Random rng = new Random();

		public List<MovingObject>  mobiles; // all objects that move
		public List<SimpleBullet> bullets; // all bullets
		public Dictionary<Type, Texture2D> textureDict; // dictionary containing loaded texturemaps
		public Tile[,] layout; // a 2D array defining the layout of the level 
		public Camera camera; // mostly contains the transformation matrix so everything fits in the viewport
		
		public Level() {
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			mobiles = new List<MovingObject>();
			textureDict = new Dictionary<Type, Texture2D>();
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize() {
			// TODO: Add your initialization logic here

			base.Initialize();
			camera = new Camera(GraphicsDevice.Viewport);
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent() {
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);
			textureDict.Add(typeof(Tile), Content.Load<Texture2D>("dirt"));
			// TODO: use this.Content to load your game content here
			LoadLevel();
		}

		// reads a level  from a file
		public void LoadLevel() {
			//load in structure bmp
			Texture2D StructTexture = Content.Load<Texture2D>("LVL0_struct");

			int W = StructTexture.Width;
			int H = StructTexture.Height;
			layout = new Tile[W, H];

			Color[] leveldata = new Color[StructTexture.Height*StructTexture.Width];
			StructTexture.GetData<Color>(leveldata);

			// add a player
			mobiles.Add(new Player(20, 20));
			textureDict.Add(typeof(Player), Content.Load<Texture2D>("Tyra-MSA"));

			// copy everything into a Color array so it's easier work with
			for (int yi = 0; yi < H; yi++) {
				for (int xi = 0; xi < W; xi++) {
					if (leveldata[yi * W + xi] == Color.White) {
						layout[xi, yi] = Tile.Air;
					}
					else if (leveldata[yi * W + xi] == Color.Red) {
						layout[xi, yi] = Tile.Air;
						mobiles.Add(new Walker(xi, yi));
						if(!textureDict.ContainsKey(typeof(Walker))) {
							textureDict.Add(typeof(Walker), Content.Load<Texture2D>("Tyra-MSA"));
						}
					}
					else if (leveldata[yi * W + xi] == Color.SpringGreen) { // that feel when green isn't 0, 255, 0

						layout[xi, yi] = Tile.Air;
						mobiles[0].x = xi;
						mobiles[0].y = yi;
					}
					else {
						layout[xi, yi] = Tile.Solid;
					}
					
				}
			}

			Texture2D LevelTexture = Content.Load<Texture2D>("LVL0");
			textureDict[typeof(Level)] = LevelTexture;
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent() {
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime) {
			// input polling is located in the Update() of objects that need it
			// TODO: Add your update logic here
			Console.WriteLine("Updating");
			foreach (MovingObject mobile in mobiles) {
				mobile.Update(gameTime, this);
			}
			camera.LocX = mobiles[0].x; //[0] should always be the primary object.
			camera.LocY = mobiles[0].y; // always the player, or if i ever implement cutscenes, wherever the camera is
			// don't need to build matrix since it isn't used in update

			base.Update(gameTime);
			Console.WriteLine("done updating");
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime) {
			Console.WriteLine("Drawing");
			// unsure if this is neccesary
			GraphicsDevice.Clear(Color.AliceBlue);

			// udate the camera matrix
			camera.BuildMatrix();
			spriteBatch.Begin(transformMatrix: camera.CameraMatrix);

			// draw the level texture
			spriteBatch.Draw(
				textureDict[typeof(Level)],
				Vector2.Zero,
				new Rectangle(0, 0, textureDict[typeof(Level)].Width, textureDict[typeof(Level)].Height),
				Color.White,
				0,
				Vector2.Zero,
				1f,
				SpriteEffects.None,
				0f);
			
			// have each moving thing draw itself
			foreach (MovingObject mobile in mobiles) {
				mobile.Draw(gameTime, spriteBatch, textureDict[mobile.GetType()]);
			}
			
			base.Draw(gameTime);
			spriteBatch.End();
		}
	}
}
